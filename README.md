API for writerslife app

 ## Build with Gradle wrapper

```sh
$ ./gradlew clean build
```

## Run with Gradle wrapper

```sh
$ ./gradlew bootRun
```

[![pipeline status](https://gitlab.com/arghya.guha/writerslife-server/badges/master/pipeline.svg)](https://gitlab.com/arghya.guha/writerslife-server/commits/master)